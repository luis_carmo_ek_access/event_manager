<?php

class ModelExtensionModuleEventManagerResendDocument extends Model
{

    public function getTotalProducts()
    {

        $query = $this->db->query("
            SELECT
                COUNT(*) AS total
            FROM
                `" . DB_PREFIX . "product` AS product
                LEFT JOIN 
                `" . DB_PREFIX . "product_description` AS product_desc
                ON (
                    product_desc.product_id = product.product_id
                )
                RIGHT JOIN 
                    `" . DB_PREFIX . "event_manager_product` AS event_manager_product
                ON (
                    event_manager_product.product_id = product.product_id
                )
            WHERE
                product_desc.language_id = '" . $this->config->get('config_language_id') . "'
        ");

        return $query->row['total'];

    }

    public function getProducts($data = array())
    {

        $sql = "
            SELECT
                product.product_id,
                event_manager_product.event_name,
                product_desc.name,
                product.status
            FROM
                `" . DB_PREFIX . "product` AS product
                LEFT JOIN 
                `" . DB_PREFIX . "product_description` AS product_desc
                ON (
                    product_desc.product_id = product.product_id
                )
                RIGHT JOIN 
                    `" . DB_PREFIX . "event_manager_product` AS event_manager_product
                ON (
                    event_manager_product.product_id = product.product_id
                )
            WHERE
                product_desc.language_id = '" . $this->config->get('config_language_id') . "'
        ";

        $sort_data = array(
            'event_manager_product.event_name',
            'product_desc.name',
            'product.status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

            $sql .= " ORDER BY " . $data['sort'];

        } else {

            $sql .= " ORDER BY event_manager_product.event_name";

        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {

            $sql .= " DESC";

        } else {

            $sql .= " ASC";

        }

        if (isset($data['start']) || isset($data['limit'])) {

            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getTotalSold($product_id)
    {

        $products = $this->getProductsAssociated($product_id);

        $query = $this->db->query("
            SELECT
                COUNT(*) AS total
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "order_event_manager_voucher` AS order_voucher
                ON (
                    order_voucher.order_product_id = order_product.order_product_id
                )
            WHERE
                orders.order_status_id IN(" . implode(", ", $this->config->get('config_complete_status')) . ")
                AND
                order_product.product_id IN (" . implode(', ', $products) . ")
        ");

        return $query->row['total'];

    }

    public function getSold($product_id, $data = array())
    {

        $products = $this->getProductsAssociated($product_id);

        $sql = "
            SELECT
                orders.order_id,
                orders.email,
                CONCAT(orders.firstname, ' ', orders.lastname) AS company,
                order_product.order_product_id,
                order_product.product_id,
                order_product.name AS product,
                order_voucher.code
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "order_event_manager_voucher` AS order_voucher
                ON (
                    order_voucher.order_product_id = order_product.order_product_id
                )
            WHERE
                orders.order_status_id IN(" . implode(", ", $this->config->get('config_complete_status')) . ")
                AND
                order_product.product_id IN (" . implode(', ', $products) . ")
        ";

        $sort_data = array(
            'company',
            'order_voucher.code',
            'orders.email'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

            $sql .= " ORDER BY " . $data['sort'];

        } else {

            $sql .= " ORDER BY order_voucher.code";

        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {

            $sql .= " DESC";

        } else {

            $sql .= " ASC";

        }

        if (isset($data['start']) || isset($data['limit'])) {

            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getProductsAssociated($product_id)
    {

        $products_associated = array();

        $products_associated[] = $product_id;

        $query = $this->db->query("
            SELECT
                associated_product_id
            FROM
                " . DB_PREFIX . "product_to_event_manager_product
            WHERE
                product_id = '" . (int)$product_id . "'
        ");

        foreach ($query->rows as $result) {
            $products_associated[] = $result['associated_product_id'];
        }

        return $products_associated;

    }

    public function getOrderProductOptions($order_id, $order_product_id)
    {

        $query = $this->db->query("
            SELECT
                order_option_id,
                name,
                value
            FROM
                `" . DB_PREFIX . "order_option`
            WHERE
                `order_id` = " . (int)$order_id . "
                AND
                `order_product_id` = " . (int)$order_product_id . "
        ");

        return $query->rows;

    }

    public function getProductOptions($product_id)
    {

        $products = $this->getProductsAssociated($product_id);

        $query = $this->db->query("
            SELECT
                DISTINCT options.option_id,
                option_desc.name
            FROM
                `" . DB_PREFIX . "product` AS product
            LEFT JOIN
                `" . DB_PREFIX . "product_option` AS product_option
                ON (
                    product_option.product_id = product.product_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "option` AS options
                ON (
                    options.option_id = product_option.option_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "option_description` AS option_desc
                ON (
                    option_desc.option_id = product_option.option_id
                )
            WHERE
                product.product_id IN (" . implode(', ', $products) . ")
                AND
                option_desc.language_id = '" . $this->config->get('config_language_id') . "'
        ");

        $options = array();

        foreach ($query->rows as $opt) {

            $options[] = $opt['name'];

        }

        return $options;

    }

    public function getProductDescription($product_id) {

        $query = $this->db->query("
            SELECT
                CONCAT(event_manager_prod.event_name, ' (', prod_desc.name, ')') AS name
            FROM
                " . DB_PREFIX . "product AS product
            LEFT JOIN
                " . DB_PREFIX . "product_description AS prod_desc
                ON (
                    prod_desc.product_id = product.product_id
                )
            LEFT JOIN
                " . DB_PREFIX . "event_manager_product AS event_manager_prod
                ON (
                    event_manager_prod.product_id = product.product_id
                )
            WHERE
                product.product_id = '" . (int)$product_id . "'
                AND
                prod_desc.language_id = '" . $this->config->get('config_language_id') . "'
        ");

        return $query->row['name'];

    }

    public function getAllSold($product_id)
    {

        $products = $this->getProductsAssociated($product_id);

        $query = $this->db->query("
            SELECT
                orders.order_id,
                orders.email,
                CONCAT(orders.firstname, ' ', orders.lastname) AS company,
                order_product.order_product_id,
                order_product.product_id,
                order_product.name AS product,
                order_voucher.code
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "order_event_manager_voucher` AS order_voucher
                ON (
                    order_voucher.order_product_id = order_product.order_product_id
                )
            WHERE
                orders.order_status_id IN(" . implode(", ", $this->config->get('config_complete_status')) . ")
                AND
                order_product.product_id IN (" . implode(', ', $products) . ")
            ORDER BY order_voucher.code ASC
        ");

        return $query->rows;

    }

    public function getProductCodeType($product_id) {

        $query = $this->db->query("
            SELECT
                code_type,
                status
            FROM
                " . DB_PREFIX . "event_manager_product
            WHERE
                product_id = '" . (int)$product_id . "'
        ");

        return ($query->num_rows > 0) ? $query->row : array();

    }

    public function resendDocument($order_id, $order_product_id, $product_id, $barcode) {

        $this->db->query("
            DELETE FROM 
                `" . DB_PREFIX . "order_event_manager_voucher`
            WHERE 
                order_id = '" . (int)$order_id . "'
                AND
                order_product_id = '" . (int)$order_product_id . "'
                AND
                product_id = '" . (int)$product_id . "'
                AND
                code = '" . (int)$barcode . "'
        ");

        $enabled = $this->db->query("
            SELECT
                *
            FROM
                " . DB_PREFIX . "event_manager_product
            WHERE
                product_id = '" . (int)$product_id . "'
        ");

        $code = $order_id . $product_id . $order_product_id;

        $code_length = strlen($code);

        $missing_chars = $enabled->row['code_length'] - $code_length;

        $missing_code = '';

        for ($num=0; $num < $missing_chars; $num++) {

            $missing_code .= rand(0,9);

        }

        $code .= $missing_code;

        $code_info = $this->getProductCodeType($product_id);

        $code_type = $code_info['code_type'];

        $this->db->query("
            INSERT INTO 
                " . DB_PREFIX . "order_event_manager_voucher
            SET 
                order_id = '" . (int)$order_id . "', 
                order_product_id = '" . (int)$order_product_id . "', 
                product_id = '" . (int)$product_id . "', 
                code = '" . $code . "', 
                date_added = NOW()
        ");

        $order_info = $this->getOrder($order_id);

        $language = new Language($order_info['language_code']);
        $language->load($order_info['language_code']);
        $language->load('extension/module/event_manager/send_document');

        $data['text_welcome']               = $language->get('text_welcome');
        $data['text_order_confirm']         = html_entity_decode(sprintf($language->get('text_order_confirm'), $enabled->row['event_name']), ENT_QUOTES, 'UTF-8');
        $data['text_total']                 = $language->get('text_total');
        $data['text_check_order']           = $language->get('text_check_order');
        $data['text_here']                  = $language->get('text_here');
        $data['text_more_informations']     = $language->get('text_more_informations');
        $data['text_voucher']               = $language->get('text_voucher');
        $data['text_voucher_description']   = $language->get('text_voucher_description');
        $data['text_corporation']           = $language->get('text_corporation');

        $data['config_event_manager_email_header_image']        = HTTPS_CATALOG . 'image/' . $this->config->get('config_event_manager_email_header_image');
        $data['config_event_manager_email_voucher_image']       = DIR_IMAGE . $this->config->get('config_event_manager_email_voucher_image');
        $data['config_event_manager_email_footer_website']      = $this->config->get('config_event_manager_email_footer_website');
        $data['config_event_manager_email_footer_email']        = $this->config->get('config_event_manager_email_footer_email');
        $data['config_event_manager_email_footer_telephone']    = $this->config->get('config_event_manager_email_footer_telephone');
        $data['config_event_manager_email_footer_background']   = $this->config->get('config_event_manager_email_footer_background');
        $data['config_event_manager_email_footer_text_color']   = $this->config->get('config_event_manager_email_footer_text_color');

        if ($order_info['customer_id']) {
            $data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_info['order_id'];
        } else {
            $data['link'] = '';
        }

        $order_products = $this->getOrderProducts($order_id, $order_product_id);

        // Attachments
        include_once('pdf.php');
        $attachments = array();

        // Products
        $data['products'] = array();

        foreach ($order_products as $order_product) {

            $option_data = array();

            $order_options = $this->getOrderOptions($order_info['order_id'], $order_product['order_product_id']);

            foreach ($order_options as $order_option) {
                if ($order_option['type'] != 'file') {
                    $value = $order_option['value'];
                } else {

                    $this->load->model('tool/upload');

                    $upload_info = $this->model_tool_upload->getUploadByCode($order_option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $order_option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            $order_attributes = $this->getProductAttributes($order_product['product_id']);

            $data['products'][] = array(
                'name' => $order_product['name'],
                'quantity' => $order_product['quantity'],
                'price' => $this->currency->format($order_product['price'] + ($this->config->get('config_tax') ? $order_product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'option' => $option_data,
                'total' => $this->currency->format($order_product['total'] + ($this->config->get('config_tax') ? ($order_product['tax'] * $order_product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
            );

            $data['code'] = $code;
            $data['product_attributes'] = $order_attributes;
            $data['product_options'] = $order_options;
            $data['corporation'] = $order_info['firstname'] . ' ' . $order_info['lastname'];

            $event_manager_description = $this->getEventManagerDescription($order_product['product_id']);

            $data['event_manager_description'] = $event_manager_description;

            $file_name = md5(rand()) . '.pdf';

            $pdf = new Pdf();

            $options = $pdf->getOptions();

            $options->set(array('isHtml5ParserEnabled' => true, 'isPhpEnabled' => true, 'isRemoteEnabled' => true));

            $pdf->setOptions($options);

            $contxt = stream_context_create([
                'ssl' => [
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                    'allow_self_signed'=> TRUE
                ]
            ]);

            $pdf->setHttpContext($contxt);

            $template = $this->load->view('extension/module/event_manager/document_template', $data);

            $template = str_replace(HTTPS_CATALOG . 'image/', DIR_IMAGE, $template);

            include_once('barcode/src/BarcodeGenerator.php');
            include_once('barcode/src/BarcodeGeneratorPNG.php');
            include_once('barcode/src/BarcodeGeneratorSVG.php');
            include_once('barcode/src/BarcodeGeneratorJPG.php');
            include_once('barcode/src/BarcodeGeneratorHTML.php');

            include_once('barcode/src/Exceptions/BarcodeException.php');
            include_once('barcode/src/Exceptions/InvalidCharacterException.php');
            include_once('barcode/src/Exceptions/InvalidCheckDigitException.php');
            include_once('barcode/src/Exceptions/InvalidFormatException.php');
            include_once('barcode/src/Exceptions/InvalidLengthException.php');
            include_once('barcode/src/Exceptions/UnknownTypeException.php');

            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();

            if ($code_info['status'] == 1 ) {

                if ($code_type == 'text') {

                    $template = str_replace("(CODE)", '#' . $code, $template);

                } elseif ($code_type == 'qrcode') {

                    $qr_name = md5(rand()) . '.png';

                    include_once('phpqrcode/qrlib.php');

                    if (!file_exists(DIR_IMAGE . $qr_name)) {
                        QRcode::png($code, DIR_IMAGE . $qr_name);
                    }

                    $template = str_replace("(CODE)", '<img src="' . HTTPS_CATALOG . 'image/' . $qr_name . '">', $template);

                } elseif ($code_type == 'TYPE_EAN_13') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_EAN_13)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_128') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_11') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_11)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_128_A') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128_A)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_128_B') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128_B)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_128_C') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128_C)) . '">', $template);

                } elseif ($code_type == 'TYPE_EAN_2') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_EAN_2)) . '">', $template);

                } elseif ($code_type == 'TYPE_EAN_5') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_EAN_5)) . '">', $template);

                } elseif ($code_type == 'TYPE_EAN_8') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_EAN_8)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_39') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_39)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_39_CHECKSUM') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_39_CHECKSUM)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_39E') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_39E)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_39E_CHECKSUM') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_39E_CHECKSUM)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODE_93') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_93)) . '">', $template);

                } elseif ($code_type == 'TYPE_STANDARD_2_5') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_STANDARD_2_5)) . '">', $template);

                } elseif ($code_type == 'TYPE_STANDARD_2_5_CHECKSUM') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_STANDARD_2_5_CHECKSUM)) . '">', $template);

                } elseif ($code_type == 'TYPE_INTERLEAVED_2_5') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_INTERLEAVED_2_5)) . '">', $template);

                } elseif ($code_type == 'TYPE_INTERLEAVED_2_5_CHECKSUM') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_INTERLEAVED_2_5_CHECKSUM)) . '">', $template);

                } elseif ($code_type == 'TYPE_UPC_A') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_UPC_A)) . '">', $template);

                } elseif ($code_type == 'TYPE_UPC_E') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_UPC_E)) . '">', $template);

                } elseif ($code_type == 'TYPE_MSI') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_MSI)) . '">', $template);

                } elseif ($code_type == 'TYPE_MSI_CHECKSUM') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_MSI_CHECKSUM)) . '">', $template);

                } elseif ($code_type == 'TYPE_POSTNET') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_POSTNET)) . '">', $template);

                } elseif ($code_type == 'TYPE_PLANET') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_PLANET)) . '">', $template);

                } elseif ($code_type == 'TYPE_RMS4CC') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_RMS4CC)) . '">', $template);

                } elseif ($code_type == 'TYPE_KIX') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_KIX)) . '">', $template);

                } elseif ($code_type == 'TYPE_IMB') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_IMB)) . '">', $template);

                } elseif ($code_type == 'TYPE_CODABAR') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODABAR)) . '">', $template);

                } elseif ($code_type == 'TYPE_PHARMA_CODE') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_PHARMA_CODE)) . '">', $template);

                } elseif ($code_type == 'TYPE_PHARMA_CODE_TWO_TRACKS') {

                    $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_PHARMA_CODE_TWO_TRACKS)) . '">', $template);

                } else {

                    $template = str_replace("(CODE)", '#' . $code, $template);

                }

            } else {

                $template = str_replace("(CODE)", '#' . $code, $template);

            }

            $pdf->loadHtml($template, 'UTF-8');

            $pdf->render();
            $file = $pdf->output();
            file_put_contents('model/extension/module/event_manager/pdf_files/' . $file_name, $file);

            array_push($attachments, $file_name);

        }

        $this->load->model('setting/setting');

        $from = $this->model_setting_setting->getSettingValue('config_email', $order_info['store_id']);

        if (!$from) {
            $from = $this->config->get('config_email');
        }

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($order_info['email']);
        $mail->setFrom($from);
        $mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));

        foreach ($attachments as $attachment) {

            $mail->AddAttachment('model/extension/module/event_manager/pdf_files/' . $attachment);

        }

        $mail->setSubject(html_entity_decode(sprintf($language->get('text_subject'), $order_info['store_name'], $order_info['order_id']), ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($this->load->view('extension/module/event_manager/resend_document', $data));
        $mail->send();

        return $code;

    }

    public function getOrder($order_id) {
        $order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
            } else {
                $language_code = $this->config->get('config_language');
            }

            return array(
                'order_id'                => $order_query->row['order_id'],
                'invoice_no'              => $order_query->row['invoice_no'],
                'invoice_prefix'          => $order_query->row['invoice_prefix'],
                'store_id'                => $order_query->row['store_id'],
                'store_name'              => $order_query->row['store_name'],
                'store_url'               => $order_query->row['store_url'],
                'customer_id'             => $order_query->row['customer_id'],
                'firstname'               => $order_query->row['firstname'],
                'lastname'                => $order_query->row['lastname'],
                'email'                   => $order_query->row['email'],
                'telephone'               => $order_query->row['telephone'],
                'custom_field'            => json_decode($order_query->row['custom_field'], true),
                'payment_firstname'       => $order_query->row['payment_firstname'],
                'payment_lastname'        => $order_query->row['payment_lastname'],
                'payment_company'         => $order_query->row['payment_company'],
                'payment_address_1'       => $order_query->row['payment_address_1'],
                'payment_address_2'       => $order_query->row['payment_address_2'],
                'payment_postcode'        => $order_query->row['payment_postcode'],
                'payment_city'            => $order_query->row['payment_city'],
                'payment_zone_id'         => $order_query->row['payment_zone_id'],
                'payment_zone'            => $order_query->row['payment_zone'],
                'payment_zone_code'       => $payment_zone_code,
                'payment_country_id'      => $order_query->row['payment_country_id'],
                'payment_country'         => $order_query->row['payment_country'],
                'payment_iso_code_2'      => $payment_iso_code_2,
                'payment_iso_code_3'      => $payment_iso_code_3,
                'payment_address_format'  => $order_query->row['payment_address_format'],
                'payment_custom_field'    => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method'          => $order_query->row['payment_method'],
                'payment_code'            => $order_query->row['payment_code'],
                'shipping_firstname'      => $order_query->row['shipping_firstname'],
                'shipping_lastname'       => $order_query->row['shipping_lastname'],
                'shipping_company'        => $order_query->row['shipping_company'],
                'shipping_address_1'      => $order_query->row['shipping_address_1'],
                'shipping_address_2'      => $order_query->row['shipping_address_2'],
                'shipping_postcode'       => $order_query->row['shipping_postcode'],
                'shipping_city'           => $order_query->row['shipping_city'],
                'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
                'shipping_zone'           => $order_query->row['shipping_zone'],
                'shipping_zone_code'      => $shipping_zone_code,
                'shipping_country_id'     => $order_query->row['shipping_country_id'],
                'shipping_country'        => $order_query->row['shipping_country'],
                'shipping_iso_code_2'     => $shipping_iso_code_2,
                'shipping_iso_code_3'     => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field'   => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method'         => $order_query->row['shipping_method'],
                'shipping_code'           => $order_query->row['shipping_code'],
                'comment'                 => $order_query->row['comment'],
                'total'                   => $order_query->row['total'],
                'order_status_id'         => $order_query->row['order_status_id'],
                'order_status'            => $order_query->row['order_status'],
                'affiliate_id'            => $order_query->row['affiliate_id'],
                'commission'              => $order_query->row['commission'],
                'language_id'             => $order_query->row['language_id'],
                'language_code'           => $language_code,
                'currency_id'             => $order_query->row['currency_id'],
                'currency_code'           => $order_query->row['currency_code'],
                'currency_value'          => $order_query->row['currency_value'],
                'ip'                      => $order_query->row['ip'],
                'forwarded_ip'            => $order_query->row['forwarded_ip'],
                'user_agent'              => $order_query->row['user_agent'],
                'accept_language'         => $order_query->row['accept_language'],
                'date_added'              => $order_query->row['date_added'],
                'date_modified'           => $order_query->row['date_modified']
            );
        } else {
            return false;
        }
    }

    public function getOrderProducts($order_id, $order_product_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

        return $query->rows;
    }

    public function getOrderOptions($order_id, $order_product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

        return $query->rows;
    }

    public function getProductAttributes($product_id) {
        
        $query = $this->db->query("
            SELECT
                attr_desc.name,
                prod_attr.text
            FROM
                " . DB_PREFIX . "product_attribute AS prod_attr
            LEFT JOIN
                " . DB_PREFIX . "attribute AS attr
                ON (
                    attr.attribute_id = prod_attr.attribute_id
                )
            LEFT JOIN
                " . DB_PREFIX . "attribute_description AS attr_desc
                ON (
                    attr_desc.attribute_id = attr.attribute_id
                )
            WHERE
                prod_attr.product_id = '" . (int)$product_id . "'
                AND
                attr_desc.language_id = '" . $this->config->get('config_language_id') . "'
                AND
                prod_attr.language_id = '" . $this->config->get('config_language_id') . "'
        ");

        return $query->rows;

    }

    public function getEventManagerDescription($product_id) {

        $query = $this->db->query("
                SELECT
                    product_id,
                    event_description
                FROM
                    " . DB_PREFIX . "event_manager_product
                WHERE
                    product_id = '" . (int)$product_id . "'
            ");

        return ($query->num_rows > 0) ? $query->row['event_description'] : '';

    }

}