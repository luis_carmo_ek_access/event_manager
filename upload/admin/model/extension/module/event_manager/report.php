<?php

class ModelExtensionModuleEventManagerReport extends Model {

    public function getTotalOrders($data = array()) {

        $sql = "
            SELECT
                COUNT(DISTINCT order_product.order_product_id) AS total
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "product_to_event_manager_partner` AS event_manager_product
                ON (
                    event_manager_product.product_id = order_product.product_id
                )
        ";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        if (!empty($data['filter_company'])) {

            $sql .= " AND CONCAT(orders.firstname, ' ', orders.lastname) LIKE '%" . $this->db->escape($data['filter_company']) . "%'";

        }

        if(!empty($data['filter_partner'])) {

            $sql .= " AND event_manager_product.partner_id = '" . $this->db->escape($data['filter_partner']) . "'";

        }

        $query = $this->db->query($sql);

        return $query->row['total'];

    }

    public function getOrders($data = array()) {

        $sql = "
            SELECT
                DISTINCT order_product.order_product_id,
                orders.order_id,
                order_product.product_id,
                order_product.name,
                order_product.price,
                order_product.tax,
                CONCAT(orders.firstname, ' ', orders.lastname) AS company,
                orders.date_added
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "product_to_event_manager_partner` AS event_manager_product
                ON (
                    event_manager_product.product_id = order_product.product_id
                )
        ";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        if (!empty($data['filter_company'])) {

            $sql .= " AND CONCAT(orders.firstname, ' ', orders.lastname) LIKE '%" . $this->db->escape($data['filter_company']) . "%'";

        }

        if(!empty($data['filter_partner'])) {

            $sql .= " AND event_manager_product.partner_id = '" . $this->db->escape($data['filter_partner']) . "'";

        }

        $sql .= " ORDER BY orders.date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getAllOrders($data = array()) {

        $sql = "
            SELECT
                DISTINCT order_product.order_product_id,
                orders.order_id,
                order_product.product_id,
                order_product.name,
                order_product.price,
                order_product.tax,
                CONCAT(orders.firstname, ' ', orders.lastname) AS company,
                orders.date_added
            FROM
                `" . DB_PREFIX . "order` AS orders
            LEFT JOIN
                `" . DB_PREFIX . "order_product` AS order_product
                ON (
                    order_product.order_id = orders.order_id
                )
            LEFT JOIN
                `" . DB_PREFIX . "product_to_event_manager_partner` AS event_manager_product
                ON (
                    event_manager_product.product_id = order_product.product_id
                )
        ";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        if (!empty($data['filter_company'])) {

            $sql .= " AND CONCAT(orders.firstname, ' ', orders.lastname) LIKE '%" . $this->db->escape($data['filter_company']) . "%'";

        }

        if(!empty($data['filter_partner'])) {

            $sql .= " AND event_manager_product.partner_id = '" . $this->db->escape($data['filter_partner']) . "'";

        }

        $sql .= " ORDER BY orders.date_added DESC";

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getOrderProductPartner($product_id) {

        $query = $this->db->query("
            SELECT
                partner.id,
                partner.name,
                partner.recipe_value,
                partner.recipe_type
            FROM
                `" . DB_PREFIX . "product_to_event_manager_partner` AS event_partner
            LEFT JOIN
                `" . DB_PREFIX . "event_manager_partner` AS partner
                ON
                    (event_partner.partner_id = partner.id)
            WHERE
                event_partner.product_id = '" . (int)$product_id . "'
        ");

        return $query->rows;

    }

}