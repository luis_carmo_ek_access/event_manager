<?php

class ModelExtensionModuleEventManagerPartner extends Model {

    public function getTotalPartners() {

        $query = $this->db->query("
            SELECT
                COUNT(*) AS total
            FROM
                " . DB_PREFIX . "event_manager_partner
        ");

        return $query->row['total'];

    }

    public function getPartners($data = array()) {

        $sql = "
            SELECT
                id AS partner_id,
                name,
                recipe_value,
                recipe_type,
                status
            FROM
                " . DB_PREFIX . "event_manager_partner
        ";

        if (!empty($data['filter_name'])) {

            $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "'%'";

        }

        $sort_data = array(
            'name',
            'recipe_value',
            'recipe_type',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

            $sql .= " ORDER BY " . $data['sort'];

        } else {

            $sql .= " ORDER BY name";

        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {

            $sql .= " DESC";

        } else {

            $sql .= " ASC";

        }

        if (isset($data['start']) || isset($data['limit'])) {

            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getPartnerByEmail($email) {

        $query = $this->db->query("
            SELECT DISTINCT
                id AS partner_id,
                address_id,
                name,
                tin,
                nib,
                email,
                telephone,
                recipe_value,
                recipe_type,
                status,
                date_added,
                modified
            FROM
                " . DB_PREFIX . "event_manager_partner
            WHERE
                LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'
        ");

        return $query->row;

    }

    public function getPartner($partner_id) {

        $query = $this->db->query("
            SELECT
                *
            FROM
                " . DB_PREFIX . "event_manager_partner
            WHERE
                id = '" . (int)$partner_id . "'
        ");

        return $query->row;

    }

    public function getAddresses($partner_id) {

        $address_data = array();

        $query = $this->db->query("
            SELECT 
                address_id 
            FROM 
                " . DB_PREFIX . "event_manager_partner_address 
            WHERE 
                partner_id = '" . (int)$partner_id . "'
        ");

        foreach ($query->rows as $result) {

            $address_info = $this->getAddress($result['address_id']);

            if ($address_info) {
                $address_data[$result['address_id']] = $address_info;
            }

        }

        return $address_data;

    }

    public function getAddress($address_id) {

        $address_query = $this->db->query("
            SELECT 
                * 
            FROM 
                 " . DB_PREFIX . "event_manager_partner_address 
            WHERE 
                address_id = '" . (int)$address_id . "'
        ");

        if ($address_query->num_rows) {

            $country_query = $this->db->query("
                SELECT 
                    * 
                FROM 
                    `" . DB_PREFIX . "country` 
                WHERE 
                    country_id = '" . (int)$address_query->row['country_id'] . "'
            ");

            if ($country_query->num_rows) {

                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];

            } else {

                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';

            }

            $zone_query = $this->db->query("
                SELECT 
                   * 
                FROM 
                    `" . DB_PREFIX . "zone` 
                WHERE 
                    zone_id = '" . (int)$address_query->row['zone_id'] . "'
            ");

            if ($zone_query->num_rows) {

                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];

            } else {

                $zone = '';
                $zone_code = '';

            }

            return array(
                'address_id'     => $address_query->row['address_id'],
                'partner_id'    => $address_query->row['partner_id'],
                'firstname'      => $address_query->row['firstname'],
                'lastname'       => $address_query->row['lastname'],
                'company'        => $address_query->row['company'],
                'address_1'      => $address_query->row['address_1'],
                'address_2'      => $address_query->row['address_2'],
                'postcode'       => $address_query->row['postcode'],
                'city'           => $address_query->row['city'],
                'zone_id'        => $address_query->row['zone_id'],
                'zone'           => $zone,
                'zone_code'      => $zone_code,
                'country_id'     => $address_query->row['country_id'],
                'country'        => $country,
                'iso_code_2'     => $iso_code_2,
                'iso_code_3'     => $iso_code_3,
                'address_format' => $address_format,
                'custom_field'   => json_decode($address_query->row['custom_field'], true)
            );

        }

    }

    public function addPartner($data) {

        $this->db->query("
            INSERT INTO 
                " . DB_PREFIX . "event_manager_partner 
            SET 
                name = '" . $this->db->escape($data['name']) . "', 
                tin = '" . $this->db->escape($data['tin']) . "', 
                nib = '" . $this->db->escape($data['nib']) . "', 
                email = '" . $this->db->escape($data['email']) . "', 
                telephone = '" . $this->db->escape($data['telephone']) . "', 
                recipe_value = '" . $this->db->escape($data['recipe_value']) . "', 
                recipe_type = '" . $this->db->escape($data['recipe_type']) . "', 
                status = '" . (int)$data['status'] . "', 
                date_added = NOW(),
                modified = NOW()
            ");

        $partner_id = $this->db->getLastId();

        if (isset($data['address'])) {

            foreach ($data['address'] as $address) {

                $this->db->query("
                    INSERT INTO 
                        " . DB_PREFIX . "event_manager_partner_address 
                    SET 
                        partner_id = '" . (int)$partner_id . "', 
                        firstname = '" . $this->db->escape($address['firstname']) . "', 
                        lastname = '" . $this->db->escape($address['lastname']) . "', 
                        company = '" . $this->db->escape($address['company']) . "', 
                        address_1 = '" . $this->db->escape($address['address_1']) . "', 
                        address_2 = '" . $this->db->escape($address['address_2']) . "', 
                        city = '" . $this->db->escape($address['city']) . "', 
                        postcode = '" . $this->db->escape($address['postcode']) . "', 
                        country_id = '" . (int)$address['country_id'] . "', 
                        zone_id = '" . (int)$address['zone_id'] . "'
                ");

                if (isset($address['default'])) {

                    $address_id = $this->db->getLastId();

                    $this->db->query("
                        UPDATE 
                            " . DB_PREFIX . "event_manager_partner 
                        SET 
                            address_id = '" . (int)$address_id . "' 
                        WHERE 
                            id = '" . (int)$partner_id . "'");

                }

            }
        }

        return $partner_id;
    }

    public function editPartner($partner_id, $data) {

        $this->db->query("
            UPDATE 
                " . DB_PREFIX . "event_manager_partner 
            SET 
                name = '" . $this->db->escape($data['name']) . "', 
                tin = '" . $this->db->escape($data['tin']) . "', 
                nib = '" . $this->db->escape($data['nib']) . "', 
                email = '" . $this->db->escape($data['email']) . "', 
                telephone = '" . $this->db->escape($data['telephone']) . "', 
                recipe_value = '" . $this->db->escape($data['recipe_value']) . "', 
                recipe_type = '" . $this->db->escape($data['recipe_type']) . "', 
                status = '" . (int)$data['status'] . "', 
                modified = NOW()
            WHERE id = '" . (int)$partner_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "event_manager_partner_address WHERE partner_id = '" . (int)$partner_id . "'");

        if (isset($data['address'])) {

            foreach ($data['address'] as $address) {

                $this->db->query("
                    INSERT INTO 
                        " . DB_PREFIX . "event_manager_partner_address 
                    SET 
                        partner_id = '" . (int)$partner_id . "', 
                        firstname = '" . $this->db->escape($address['firstname']) . "', 
                        lastname = '" . $this->db->escape($address['lastname']) . "', 
                        company = '" . $this->db->escape($address['company']) . "', 
                        address_1 = '" . $this->db->escape($address['address_1']) . "', 
                        address_2 = '" . $this->db->escape($address['address_2']) . "', 
                        city = '" . $this->db->escape($address['city']) . "', 
                        postcode = '" . $this->db->escape($address['postcode']) . "', 
                        country_id = '" . (int)$address['country_id'] . "', 
                        zone_id = '" . (int)$address['zone_id'] . "'
                ");

                if (isset($address['default'])) {

                    $address_id = $this->db->getLastId();

                    $this->db->query("
                        UPDATE 
                            " . DB_PREFIX . "event_manager_partner 
                        SET 
                            address_id = '" . (int)$address_id . "' 
                        WHERE 
                            id = '" . (int)$partner_id . "'");

                }

            }

        }

    }

    public function deletePartner($partner_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "event_manager_partner WHERE id = '" . (int)$partner_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "event_manager_partner_address WHERE partner_id = '" . (int)$partner_id . "'");

    }

    public function isPartnerSystem($partner_id) {

        $query = $this->db->query("
            SELECT
                system
            FROM
                " . DB_PREFIX . "event_manager_partner
            WHERE
                id = '" . $this->db->escape($partner_id) . "'
        ");

        return $query->row;

    }

}