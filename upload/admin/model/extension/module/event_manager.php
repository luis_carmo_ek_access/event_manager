<?php

class ModelExtensionModuleEventManager extends Model {

    private static $TABLES = array(
        'event_manager_partner' => '
			CREATE TABLE IF NOT EXISTS `%s` (
				`id` INT(11) NOT NULL AUTO_INCREMENT,
				`address_id` INT(11),
                `name` varchar(128) NOT NULL,
				`tin` varchar(64) NOT NULL,
				`nib` varchar(64) NOT NULL,
				`email`  varchar(96) NOT NULL,
				`telephone` varchar(32),
				`recipe_value` DECIMAL( 10, 2 ) NOT NULL,
				`recipe_type` varchar(16) NOT NULL,
				`status` tinyint(1) NOT NULL DEFAULT \'1\',
				`system` tinyint(1) NOT NULL DEFAULT \'0\',
				`date_added` DATETIME NOT NULL,
				`modified` DATETIME NOT NULL,
				PRIMARY KEY `id` (`id`)
        	) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		',
        'event_manager_partner_address' => '
			CREATE TABLE IF NOT EXISTS `%s` (
                `address_id` int(11) NOT NULL,
                `partner_id` int(11) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `company` varchar(40) NOT NULL,
                `address_1` varchar(128) NOT NULL,
                `address_2` varchar(128) NOT NULL,
                `city` varchar(128) NOT NULL,
                `postcode` varchar(10) NOT NULL,
                `country_id` int(11) NOT NULL DEFAULT \'0\',
                `zone_id` int(11) NOT NULL DEFAULT \'0\'
        	) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ',
        'product_to_event_manager_partner' => '
            CREATE TABLE IF NOT EXISTS `%s` (
                `product_id` int(11) NOT NULL,
                `partner_id` int(11) NOT NULL DEFAULT \'0\'
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ',
        'product_to_event_manager_product' => '
            CREATE TABLE IF NOT EXISTS `%s` (
                `product_id` int(11) NOT NULL,
                `associated_product_id` int(11) NOT NULL DEFAULT \'0\'
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ',
        'event_manager_product' => '
            CREATE TABLE IF NOT EXISTS `%s` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `product_id` int(11) NOT NULL,
                `code_type` varchar(32) NOT NULL DEFAULT \'ean-13\',
                `code_length` int NOT NULL DEFAULT \'13\',
                `event_name` varchar(254) NOT NULL DEFAULT \'Event\',
                `event_description` text NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT \'0\',
                PRIMARY KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ',
        'order_event_manager_voucher' => '
            CREATE TABLE IF NOT EXISTS `%s` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `order_id` int(11) NOT NULL,
                `order_product_id` int(11) NOT NULL,
                `product_id` int(11) NOT NULL,
                `code` varchar(254) NOT NULL DEFAULT \'\' UNIQUE,
                `date_added` DATETIME NOT NULL,
                PRIMARY KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ',
    );

    public function database() {

        foreach (self::$TABLES as $name => $sql) {
            $this->db->query(sprintf($sql, DB_PREFIX . $name));
        }

        $this->db->query("
            INSERT INTO " . DB_PREFIX . "event_manager_partner(
                `id`,
                `name`,
                `tin`,
                `nib`,
                `email`,
                `telephone`,
                `recipe_value`,
                `recipe_type`,
                `status`,
                `system`,
                `date_added`,
                `modified`
            )
            VALUES(
                NULL,
                'Exclusivkey',
                '000000',
                '000000000',
                'luis.carmo@exclusivkey.pt',
                NULL,
                '2',
                'percentage',
                '1',
                '1',
                NOW(),
                NOW()
            );
        ");

    }

    public function install() {

        $this->database();

        $this->load->model('user/user_group');

        $files = glob(DIR_APPLICATION . 'model/extension/module/event_manager/*.php');
        $data = $this->model_user_user_group->getUserGroup($this->user->getGroupId());

        foreach ($files as $file) {
            $file = 'extension/module/event_manager/' . str_replace('.php', '', basename($file));

            $data['permission']['access'][] = $file;
            $data['permission']['modify'][] = $file;
        }

        $data['permission']['access'] = array_unique($data['permission']['access']);
        $data['permission']['modify'] = array_unique($data['permission']['modify']);

        $this->model_user_user_group->editUserGroup($this->user->getGroupId(), $data);

        $this->setConfigs();

    }

    public function uninstall() {

        foreach (self::$TABLES as $name => $sql) {
            $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . $name . "`");
        }

        $this->removeConfigs();

    }

    private function setConfigs() {

       $this->db->query("
            INSERT INTO " . DB_PREFIX . "setting(
                `code`,
                `key`, 
                `value`,
                `serialized`
            )
            VALUES
                (
                    'config',
                    'config_event_manager',
                    '1',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_code_type',
                    'ean-13',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_code_length',
                    '13',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_header_image',
                    '/image/no_image.png',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_voucher_image',
                    '/image/no_image.png',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_footer_website',
                    'website.com',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_footer_email',
                    'mail@mail.com',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_footer_telephone',
                    '+351000000000',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_footer_background',
                    '#42BBBA',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_email_footer_text_color',
                    '#ffffff',
                    '0'
                ),
                (
                    'config',
                    'config_event_manager_emails',
                    '1',
                    '0'
                );
       ");

    }

    private function removeConfigs() {

        $this->db->query("
            DELETE FROM " . DB_PREFIX . "setting
            WHERE
                `key` LIKE '%config_event_manager%'
        ");

    }

}