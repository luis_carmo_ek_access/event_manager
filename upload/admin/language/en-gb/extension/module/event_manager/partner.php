<?php

// Heading
$_['heading_title']             = 'Partners';

// Columns
$_['column_name']               = 'Name';
$_['column_recipe_value']       = 'Commission Value';
$_['column_recipe_type']        = 'Commission Type';
$_['column_status']             = 'Status';
$_['column_action']             = 'Action';

// Tabs
$_['tab_general']               = 'General';
$_['tab_address']               = 'Address';

// Button
$_['button_address_add']        = 'Add Address';

// Text
$_['text_success']              = 'Partner(s) Modified With Success!';
$_['text_percentage']           = 'Percentage';
$_['text_fix']                  = 'Fix';
$_['text_disabled']             = 'Disabled';
$_['text_enabled']              = 'Enabled';
$_['text_add']                  = 'Add';
$_['text_edit']                 = 'Edit';
$_['text_account']              = 'Partner Details';

// Entries
$_['entry_name']                = 'Name';
$_['entry_tin']                 = 'TIN';
$_['entry_nib']                 = 'NIB';
$_['entry_recipe_value']        = 'Recipe Value';
$_['entry_recipe_type']         = 'Recipe Type';
$_['entry_status']              = 'Status';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telephone';
$_['entry_firstname']           = 'First Name';
$_['entry_lastname']            = 'Last Name';
$_['entry_company']             = 'Company';
$_['entry_address_1']           = 'Address 1';
$_['entry_address_2']           = 'Address 2';
$_['entry_city']                = 'City';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'Region / State';
$_['entry_default']             = 'Default Address';

// Errors
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_name']                = 'Name must be between 3 and 128 characters!';
$_['error_tin']                 = 'TIN must be between 3 and 64 characters!';
$_['error_nib']                 = 'Name must be between 3 and 64 characters!';
$_['recipe_value']              = 'Recipe Value must be numeric!';
$_['recipe_type']               = 'Recipe Type Invalid!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be between 2 and 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_custom_field']        = '%s required!';
$_['error_system_partner']      = 'One or More Users Can\'t Be Deleted Due To System Requirements!';