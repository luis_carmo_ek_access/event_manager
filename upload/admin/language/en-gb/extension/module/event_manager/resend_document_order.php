<?php

// Heading
$_['heading_title']             = 'List Event Documents';

// Columns
$_['column_name']               = 'Name';
$_['column_code']               = 'Code';
$_['column_email']              = 'E-mail';
$_['column_action']             = 'Resend';
$_['column_product']            = 'Product';

// Tabs
$_['tab_general']               = 'General';

// Button
$_['button_view']               = 'View';
$_['button_resend']             = 'Resend';
$_['button_print']              = 'Print List';

// Text
$_['text_success']              = 'Partner(s) Modified With Success!';
$_['text_disabled']             = 'Disabled';
$_['text_enabled']              = 'Enabled';
$_['text_resent_successfully']  = 'Resent Successfully!';

// Entries
$_['entry_name']                = 'Name';
$_['entry_tin']                 = 'TIN';
$_['entry_nib']                 = 'NIB';
$_['entry_recipe_value']        = 'Recipe Value';
$_['entry_recipe_type']         = 'Recipe Type';
$_['entry_status']              = 'Status';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telephone';
$_['entry_firstname']           = 'First Name';
$_['entry_lastname']            = 'Last Name';
$_['entry_company']             = 'Company';
$_['entry_address_1']           = 'Address 1';
$_['entry_address_2']           = 'Address 2';
$_['entry_city']                = 'City';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'Region / State';
$_['entry_default']             = 'Default Address';

// Errors
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_product_invalid']     = 'Warning: Product Invalid!';
$_['error_invalid']             = 'Warning: Invalid Params!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_name']                = 'Name must be between 3 and 128 characters!';
$_['error_tin']                 = 'TIN must be between 3 and 64 characters!';
$_['error_nib']                 = 'Name must be between 3 and 64 characters!';
$_['recipe_value']              = 'Recipe Value must be numeric!';
$_['recipe_type']               = 'Recipe Type Invalid!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be between 2 and 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_custom_field']        = '%s required!';
$_['error_system_partner']      = 'One or More Users Can\'t Be Deleted Due To System Requirements!';