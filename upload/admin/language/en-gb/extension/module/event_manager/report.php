<?php

// Heading
$_['heading_title']     = 'Reports';

// Button
$_['button_export_csv'] = 'Export CSV';

// Text
$_['text_simple']       = 'Simple';
$_['text_detailed']     = 'Detailed';
$_['text_filter']       = 'Filter';
$_['text_year']         = 'Years';
$_['text_month']        = 'Months';
$_['text_week']         = 'Weeks';
$_['text_day']          = 'Days';
$_['text_all_status']   = 'All Statuses';
$_['text_all_partners'] = 'All Partners';

// Column
$_['column_date_time']  = 'Date Time';
$_['column_product_name'] = 'Product Name';
$_['column_price']      = 'Price';
$_['column_company']    = 'Company';
$_['column_partner']    = 'Partner';
$_['column_options']    = 'Options';
$_['column_date_start'] = 'Date Start';
$_['column_date_end']   = 'Date End';
$_['column_orders']     = 'No. Orders';
$_['column_products']   = 'No. Products';
$_['column_tax']        = 'Tax';
$_['column_total']      = 'Total';
$_['column_store']      = 'Store';
$_['column_partners']   = 'Partners';

// Entry
$_['entry_type']        = 'Report Type';
$_['entry_date_start']  = 'Date Start';
$_['entry_date_end']    = 'Date End';
$_['entry_group']       = 'Group By';
$_['entry_status']      = 'Order Status';
$_['entry_status']      = 'Status';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_partner']     = 'Partner';
$_['entry_company']     = 'Company';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify sales report!';