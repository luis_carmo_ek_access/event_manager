<?php

$_['heading_title']     = 'Event Manager';
$_['heading_partner']   = 'Partners';
$_['heading_resend_document']   = 'Documents';
$_['heading_report']    = 'Reports';

$_['entry_name']        = 'Module ID';
$_['entry_status']      = 'Status';;

$_['error_warning']     = 'There are problems with data provided';
$_['error_permission']  = 'You don\'t have permission to edit this.';

$_['button_save']       = 'Save';
$_['button_cancel']     = 'Cancel';

$_['text_extension']    = 'Extensions';
$_['text_edit']         = 'Edit';
$_['text_success']      = 'Saved';