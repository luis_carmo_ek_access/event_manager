<?php

// Heading
$_['heading_title']             = 'Parceiros';

// Columns
$_['column_name']               = 'Nome';
$_['column_recipe_value']       = 'Valor Comissão';
$_['column_recipe_type']        = 'Tipo Comissão';
$_['column_status']             = 'Estado';
$_['column_action']             = 'Ação';

// Tabs
$_['tab_general']               = 'Geral';
$_['tab_address']               = 'Morada';

// Button
$_['button_address_add']        = 'Adicionar Morada';

// Text
$_['text_success']              = 'Parceiro(s) Modificados Com Sucesso!';
$_['text_percentage']           = 'Percentagem';
$_['text_fix']                  = 'Fixo';
$_['text_disabled']             = 'Desativo';
$_['text_enabled']              = 'Ativo';
$_['text_add']                  = 'Adicionar';
$_['text_edit']                 = 'Editar';
$_['text_account']              = 'Detalhes Parceiro';

// Entries
$_['entry_name']                = 'Nome';
$_['entry_tin']                 = 'NIF';
$_['entry_nib']                 = 'NIB';
$_['entry_recipe_value']        = 'Valor Receber';
$_['entry_recipe_type']         = 'Tipo Valor';
$_['entry_status']              = 'Estado';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telefone';
$_['entry_firstname']           = 'Primeiro Nome';
$_['entry_lastname']            = 'Último Nome';
$_['entry_company']             = 'Empresa';
$_['entry_address_1']           = 'Morada 1';
$_['entry_address_2']           = 'Morada 2';
$_['entry_city']                = 'Cidade';
$_['entry_postcode']            = 'Código Postal';
$_['entry_country']             = 'País';
$_['entry_zone']                = 'Região / Estado';
$_['entry_default']             = 'Morada Padrão';

// Errors
$_['error_warning']             = 'Aviso: Por favor verifique cuidadosamente os erros!';
$_['error_permission']          = 'Aviso: Não tem permissões para modificar os parceiros!';
$_['error_exists']              = 'Aviso: O E-Mail já se encontra registado!';
$_['error_name']                = 'O Nome deverá ter entre 3 a 128 caracteres!';
$_['error_tin']                 = 'O NIF  deverá ter entre 3 a 64 caracteres!';
$_['error_nib']                 = 'O NIB  deverá ter entre 3 a 64 caracteres!';
$_['recipe_value']              = 'O Valor a Receber deverá ser numérico!';
$_['recipe_type']               = 'Tipo Valor Inválido!';
$_['error_firstname']           = 'O Primeiro Nome deverá ter entre 3 a 32 caracteres!';
$_['error_lastname']            = 'O Último Nome deverá ter entre 3 a 32 caracteres!';
$_['error_email']               = 'E-Mail Inválido!';
$_['error_telephone']           = 'O Telefone deverá ter entre 3 a 32 caracteres!';
$_['error_address_1']           = 'A Morada deverá ter entre 3 a 128 caracteres!';
$_['error_city']                = 'A Cidade deverá ter entre 3 a 128 caracteres!';
$_['error_postcode']            = 'O Código Postal deverá ter entre 2 a 10 caracteres para este país!';
$_['error_country']             = 'Por favor selecione um país!';
$_['error_zone']                = 'Por favor selecione uma região / estado!';
$_['error_custom_field']        = '%s obrigatório!';