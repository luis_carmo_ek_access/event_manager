<?php

$_['heading_title']         = 'Gestor Eventos';
$_['heading_partner']       = 'Parceiros';

$_['entry_name']            = 'ID Modulo';
$_['entry_status']          = 'Estado';;

$_['error_warning']         = 'Ocorreu um erro com os dados fornecidos!';
$_['error_permission']      = 'Atualmente não tem permissões para editar o modulo.';

$_['button_save']           = 'Guardar';
$_['button_cancel']         = 'Cancelar';

$_['text_extension']        = 'Extensões';
$_['text_edit']             = 'Editar';
$_['text_success']          = 'Sucesso: As definições foram guardadas!';