<?php

class ControllerExtensionModuleEventManagerPartner extends Controller {

    private $error = array();

    public function index() {

        $this->load->language('extension/module/event_manager/partner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/partner');

        $this->getList();

    }

    protected function getList() {

        if (isset($this->request->get['sort'])) {

            $sort = $this->request->get['sort'];

        } else {

            $sort = 'name';

        }

        if (isset($this->request->get['order'])) {

            $order = $this->request->get['order'];

        } else {

            $order = 'ASC';

        }

        if (isset($this->request->get['page'])) {

            $page = $this->request->get['page'];

        } else {

            $page = 1;

        }

        $url = '';

        if (isset($this->request->get['sort'])) {

            $url .= '&sort=' . $this->request->get['sort'];

        }

        if (isset($this->request->get['order'])) {

            $url .= '&order=' . $this->request->get['order'];

        }

        if (isset($this->request->get['page'])) {

            $url .= '&page=' . $this->request->get['page'];

        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('extension/module/event_manager/partner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('extension/module/event_manager/partner/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['partners'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $partners_total = $this->model_extension_module_event_manager_partner->getTotalPartners();

        $results = $this->model_extension_module_event_manager_partner->getPartners($filter_data);

        foreach ($results as $result) {
            $data['partners'][] = array(
                'partner_id'       => $result['partner_id'],
                'name'              => $result['name'],
                'recipe_value'      => $result['recipe_value'],
                'recipe_type'       => $this->language->get('text_' . $result['recipe_type']),
                'status'            => ($result['status'] == 1) ? $this->language->get('text_enabled') : $this->language->get('text_disabled') ,
                'edit'              => $this->url->link('extension/module/event_manager/partner/edit', 'user_token=' . $this->session->data['user_token'] . '&partner_id=' . $result['partner_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_recipe_value'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . '&sort=recipe_value' . $url, true);
        $data['sort_recipe_type'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . '&sort=recipe_type' . $url, true);
        $data['sort_status'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);
        $data['sort_sort_order'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $partners_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($partners_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($partners_total - $this->config->get('config_limit_admin'))) ? $partners_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $partners_total, ceil($partners_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager/partner_list', $data));
    }

    public function add() {

        $this->load->language('extension/module/event_manager/partner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/partner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_extension_module_event_manager_partner->addPartner($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true));

        }

        $this->getForm();

    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'extension/module/event_manager/partner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen(trim($this->request->post['name'])) > 128)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        $partner_info = $this->model_extension_module_event_manager_partner->getPartnerByEmail($this->request->post['email']);

        if (!isset($this->request->get['partner_id'])) {

            if ($partner_info) {
                $this->error['warning'] = $this->language->get('error_exists');
            }

        } else {

            if ($partner_info && ($this->request->get['partner_id'] != $partner_info['partner_id'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }

        }

        if (!empty($this->request->post['telephone'])) {

            if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                $this->error['telephone'] = $this->language->get('error_telephone');
            }

        }

        if ((utf8_strlen($this->request->post['tin']) < 3) || (utf8_strlen($this->request->post['tin']) > 32)) {
            $this->error['tin'] = $this->language->get('error_tin');
        }

        if ((utf8_strlen($this->request->post['nib']) < 3) || (utf8_strlen($this->request->post['nib']) > 32)) {
            $this->error['nib'] = $this->language->get('error_nib');
        }

        if (!is_numeric($this->request->post['recipe_value'])) {
            $this->error['recipe_value'] = $this->language->get('error_recipe_value');
        }

        $recipe_types = [
            'percentage',
            'fix'
        ];

        if (!in_array($this->request->post['recipe_type'], $recipe_types)) {
            $this->error['recipe_type'] = $this->language->get('error_recipe_type');
        }

        if (isset($this->request->post['address'])) {

            foreach ($this->request->post['address'] as $key => $value) {

                if ((utf8_strlen($value['firstname']) < 1) || (utf8_strlen($value['firstname']) > 32)) {
                    $this->error['address'][$key]['firstname'] = $this->language->get('error_firstname');
                }

                if ((utf8_strlen($value['lastname']) < 1) || (utf8_strlen($value['lastname']) > 32)) {
                    $this->error['address'][$key]['lastname'] = $this->language->get('error_lastname');
                }

                if ((utf8_strlen($value['address_1']) < 3) || (utf8_strlen($value['address_1']) > 128)) {
                    $this->error['address'][$key]['address_1'] = $this->language->get('error_address_1');
                }

                if ((utf8_strlen($value['city']) < 2) || (utf8_strlen($value['city']) > 128)) {
                    $this->error['address'][$key]['city'] = $this->language->get('error_city');
                }

                $this->load->model('localisation/country');

                $country_info = $this->model_localisation_country->getCountry($value['country_id']);

                if ($country_info && $country_info['postcode_required'] && (utf8_strlen($value['postcode']) < 2 || utf8_strlen($value['postcode']) > 10)) {
                    $this->error['address'][$key]['postcode'] = $this->language->get('error_postcode');
                }

                if ($value['country_id'] == '') {
                    $this->error['address'][$key]['country'] = $this->language->get('error_country');
                }

                if (!isset($value['zone_id']) || $value['zone_id'] == '') {
                    $this->error['address'][$key]['zone'] = $this->language->get('error_zone');
                }

            }

        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;

    }

    protected function getForm() {

        $data['text_form'] = !isset($this->request->get['partner_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['partner_id'])) {
            $data['partner_id'] = $this->request->get['partner_id'];
        } else {
            $data['partner_id'] = 0;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['tin'])) {
            $data['error_tin'] = $this->error['tin'];
        } else {
            $data['error_tin'] = '';
        }

        if (isset($this->error['nib'])) {
            $data['error_nib'] = $this->error['nib'];
        } else {
            $data['error_nib'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['recipe_value'])) {
            $data['error_recipe_value'] = $this->error['recipe_value'];
        } else {
            $data['error_recipe_value'] = '';
        }

        if (isset($this->error['recipe_type'])) {
            $data['error_recipe_type'] = $this->error['recipe_type'];
        } else {
            $data['error_recipe_type'] = '';
        }

        if (isset($this->error['address'])) {
            $data['error_address'] = $this->error['address'];
        } else {
            $data['error_address'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['partner_id'])) {
            $data['action'] = $this->url->link('extension/module/event_manager/partner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('extension/module/event_manager/partner/edit', 'user_token=' . $this->session->data['user_token'] . '&partner_id=' . $this->request->get['partner_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['partner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $partner_info = $this->model_extension_module_event_manager_partner->getPartner($this->request->get['partner_id']);
        }
        
        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($partner_info)) {
            $data['name'] = $partner_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['tin'])) {
            $data['tin'] = $this->request->post['tin'];
        } elseif (!empty($partner_info)) {
            $data['tin'] = $partner_info['tin'];
        } else {
            $data['tin'] = '';
        }

        if (isset($this->request->post['nib'])) {
            $data['nib'] = $this->request->post['nib'];
        } elseif (!empty($partner_info)) {
            $data['nib'] = $partner_info['nib'];
        } else {
            $data['nib'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($partner_info)) {
            $data['email'] = $partner_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } elseif (!empty($partner_info)) {
            $data['telephone'] = $partner_info['telephone'];
        } else {
            $data['telephone'] = '';
        }

        if (isset($this->request->post['recipe_value'])) {
            $data['recipe_value'] = $this->request->post['recipe_value'];
        } elseif (!empty($partner_info)) {
            $data['recipe_value'] = $partner_info['recipe_value'];
        } else {
            $data['recipe_value'] = '';
        }

        if (isset($this->request->post['recipe_type'])) {
            $data['recipe_type'] = $this->request->post['recipe_type'];
        } elseif (!empty($partner_info)) {
            $data['recipe_type'] = $partner_info['recipe_type'];
        } else {
            $data['recipe_type'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($partner_info)) {
            $data['status'] = $partner_info['status'];
        } else {
            $data['status'] = true;
        }

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        if (isset($this->request->post['address'])) {
            $data['addresses'] = $this->request->post['address'];
        } elseif (isset($this->request->get['partner_id'])) {
            $data['addresses'] = $this->model_extension_module_event_manager_partner->getAddresses($this->request->get['partner_id']);
        } else {
            $data['addresses'] = array();
        }

        if (isset($this->request->post['address_id'])) {
            $data['address_id'] = $this->request->post['address_id'];
        } elseif (!empty($partner_info)) {
            $data['address_id'] = !empty($partner_info['address_id']) ? $partner_info['address_id'] : '';
        } else {
            $data['address_id'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager/partner_form', $data));
    }

    public function edit() {

        $this->load->language('extension/module/event_manager/partner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/partner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_extension_module_event_manager_partner->editPartner($this->request->get['partner_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true));

        }

        $this->getForm();
    }

    public function delete() {

        $this->load->language('extension/module/event_manager/partner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/partner');

        if (isset($this->request->post['selected']) && $this->validateDelete($this->request->post['selected'])) {

            foreach ($this->request->post['selected'] as $partner_id) {
                $this->model_extension_module_event_manager_partner->deletePartner($partner_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();

    }

    protected function validateDelete($selected_partners) {

        if (!$this->user->hasPermission('modify', 'extension/module/event_manager/partner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($selected_partners as $partner_id) {

            $is_system = $this->model_extension_module_event_manager_partner->isPartnerSystem($partner_id);

            if ($is_system['system'] == 1) {

                $this->error['warning'] = $this->language->get('error_system_partner');

            }

        }

        return !$this->error;

    }

}