<?php

class ControllerExtensionModuleEventManagerResendDocument extends Controller {

    private $error = array();

    public function index() {

        $this->load->language('extension/module/event_manager/resend_document');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/resend_document');

        $this->getList();

    }

    protected function getList() {

        if (isset($this->request->get['sort'])) {

            $sort = $this->request->get['sort'];

        } else {

            $sort = 'name';

        }

        if (isset($this->request->get['order'])) {

            $order = $this->request->get['order'];

        } else {

            $order = 'ASC';

        }

        if (isset($this->request->get['page'])) {

            $page = $this->request->get['page'];

        } else {

            $page = 1;

        }

        $url = '';

        if (isset($this->request->get['sort'])) {

            $url .= '&sort=' . $this->request->get['sort'];

        }

        if (isset($this->request->get['order'])) {

            $url .= '&order=' . $this->request->get['order'];

        }

        if (isset($this->request->get['page'])) {

            $url .= '&page=' . $this->request->get['page'];

        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['products'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $products_total = $this->model_extension_module_event_manager_resend_document->getTotalProducts();

        $results = $this->model_extension_module_event_manager_resend_document->getProducts($filter_data);

        foreach ($results as $result) {
            $data['products'][] = array(
                'product_id'    => $result['product_id'],
                'event_name'    => $result['event_name'],
                'name'          => $result['name'],
                'status'        => ($result['status'] == 1) ? $this->language->get('text_enabled') : $this->language->get('text_disabled') ,
                'print_list'    => $this->url->link('extension/module/event_manager/resend_document/print', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $result['product_id'] . $url, true),
                'resend_list'   => $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $result['product_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {

            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);

        } else {

            $data['success'] = '';

        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_event_name'] = $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . '&sort=event_manager_product.event_name' . $url, true);
        $data['sort_name'] = $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . '&sort=product_desc.name' . $url, true);
        $data['sort_status'] = $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . '&sort=product.status' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $products_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($products_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($products_total - $this->config->get('config_limit_admin'))) ? $products_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $products_total, ceil($products_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager/resend_document_list', $data));

    }

    public function view() {

        $this->load->language('extension/module/event_manager/resend_document_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/resend_document');

        if (!isset($this->request->get['product_id'])) {

            $this->error['warning'] = $this->language->get('error_product_invalid');

            $this->getList();

        }

        if (isset($this->request->get['sort'])) {

            $sort = $this->request->get['sort'];

        } else {

            $sort = 'name';

        }

        if (isset($this->request->get['order'])) {

            $order = $this->request->get['order'];

        } else {

            $order = 'ASC';

        }

        if (isset($this->request->get['page'])) {

            $page = $this->request->get['page'];

        } else {

            $page = 1;

        }

        $url = '';

        if (isset($this->request->get['product_id'])) {

            $url .= '&product_id=' . $this->request->get['product_id'];

        }

        $data['product_id'] = $this->request->get['product_id'];
        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['sort'])) {

            $url .= '&sort=' . $this->request->get['sort'];

        }

        if (isset($this->request->get['order'])) {

            $url .= '&order=' . $this->request->get['order'];

        }

        if (isset($this->request->get['page'])) {

            $url .= '&page=' . $this->request->get['page'];

        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );


        $data['cancel'] = $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['sales'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $sold_total = $this->model_extension_module_event_manager_resend_document->getTotalSold($this->request->get['product_id']);

        $data['text_list'] = $this->model_extension_module_event_manager_resend_document->getProductDescription($this->request->get['product_id']);

        $results = $this->model_extension_module_event_manager_resend_document->getSold($this->request->get['product_id'], $filter_data);

        foreach ($results as $result) {

            $options = $this->model_extension_module_event_manager_resend_document->getOrderProductOptions($result['order_id'], $result['order_product_id']);

            $data['sales'][] = array(
                'code'              => $result['code'],
                'email'             => $result['email'],
                'name'              => $result['company'],
                'product'           => $result['product'],
                'options'           => $options,
                'resend'            => $this->url->link('extension/module/event_manager/resend_document/resend', 'user_token=' . $this->session->data['user_token'] . '&code=' . $result['code'] . $url, true),
                'order_id'          => $result['order_id'],
                'order_product_id'  => $result['order_product_id'],
                'product_id'        => $result['product_id']
            );

        }

        $data['product_options'] = $this->model_extension_module_event_manager_resend_document->getProductOptions($this->request->get['product_id']);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['product_id'])) {

            $url .= '&product_id=' . $this->request->get['product_id'];

        }

        $data['logo'] = HTTP_CATALOG . 'image/' . $this->config->get('config_logo');

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_code'] = $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . '&sort=order_voucher.code' . $url, true);
        $data['sort_email'] = $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . '&sort=orders.email' . $url, true);

        $url = '';

        if (isset($this->request->get['product_id'])) {

            $url .= '&product_id=' . $this->request->get['product_id'];

        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $sold_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('extension/module/event_manager/resend_document/view', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($sold_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sold_total - $this->config->get('config_limit_admin'))) ? $sold_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sold_total, ceil($sold_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager/resend_document_list_order', $data));


    }

    public function loadAllOrders() {

        $json = array();

        if (!isset($this->request->post['product_id'])) {

            $json['error']['value'] = true;
            $json['error']['message'] = $this->language->get('error_product_invalid');

        } else {

            $this->load->model('extension/module/event_manager/resend_document');

            $results = $this->model_extension_module_event_manager_resend_document->getAllSold($this->request->post['product_id']);

            $sales = array();

            foreach ($results as $result) {

                $options = $this->model_extension_module_event_manager_resend_document->getOrderProductOptions($result['order_id'], $result['order_product_id']);

                $sales[] = array(
                    'code'              => $result['code'],
                    'email'             => $result['email'],
                    'name'              => $result['company'],
                    'product'           => $result['product'],
                    'options'           => $options
                );

            }

            $json['success']['options'] = $this->model_extension_module_event_manager_resend_document->getProductOptions($this->request->post['product_id']);

            $json['success']['value'] = true;
            $json['success']['data'] = $sales;

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function resendDocument() {

        $this->load->language('extension/module/event_manager/resend_document');

        $json = array();

        if (!isset($this->request->post['order_id']) && !isset($this->request->post['order_product_id']) && !isset($this->request->post['product_id']) && !isset($this->request->post['barcode'])) {

            $json['error']['value'] = true;
            $json['error']['message'] = $this->language->get('error_invalid');

        } else {

            $this->load->model('extension/module/event_manager/resend_document');

            $code = $this->model_extension_module_event_manager_resend_document->resendDocument($this->request->post['order_id'], $this->request->post['order_product_id'], $this->request->post['product_id'], $this->request->post['barcode']);

            $json = array();
            $json['success']['value'] = true;
            $json['success']['message'] = $this->language->get('text_resent_successfully');
            $json['success']['data'] = $code;

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

}