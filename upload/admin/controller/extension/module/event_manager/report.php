<?php

class ControllerExtensionModuleEventManagerReport extends Controller {

    private $error = array();

    public function index() {

        $this->load->language('extension/module/event_manager/report');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module/event_manager/report');

        $this->getList();

    }

    protected function getList() {

        $this->load->language('extension/module/event_manager/report');

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = date('Y-m-d');
        }

        if (isset($this->request->get['filter_report_type'])) {

            $filter_report_type = $this->request->get['filter_report_type'];

        } else {

            $filter_report_type = 'simple';

        }

        if (isset($this->request->get['filter_company'])) {

            $filter_company = $this->request->get['filter_company'];

        } else {

            $filter_company = '';

        }

        if (isset($this->request->get['filter_partner'])) {

            $filter_partner = $this->request->get['filter_partner'];

        } else {

            $filter_partner = 0;

        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $filter_order_status_id = $this->request->get['filter_order_status_id'];
        } else {
            $filter_order_status_id = 0;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->load->model('extension/report/sale');

        $data['orders'] = array();

        $filter_data = array(
            'filter_date_start'	     => $filter_date_start,
            'filter_date_end'	     => $filter_date_end,
            'filter_report_type'	 => $filter_report_type,
            'filter_company'	     => $filter_company,
            'filter_partner'	     => $filter_partner,
            'filter_order_status_id' => $filter_order_status_id,
            'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                  => $this->config->get('config_limit_admin')
        );

        $order_total = $this->model_extension_module_event_manager_report->getTotalOrders($filter_data);

        $results = $this->model_extension_module_event_manager_report->getOrders($filter_data);

        $this->load->model('extension/module/event_manager/resend_document');

        foreach ($results as $result) {

            $options = $this->model_extension_module_event_manager_resend_document->getOrderProductOptions($result['order_id'], $result['order_product_id']);

            $partners = $this->model_extension_module_event_manager_report->getOrderProductPartner($result['product_id']);

            $data['orders'][] = array(
                'datetime'      => $result['date_added'],
                'name'          => $result['name'],
                'tax'           => $this->currency->format($result['tax'], $this->config->get('config_currency')),
                'price'         => $this->currency->format($result['price'], $this->config->get('config_currency')),
                'company'       => $result['company'],
                'options'       => $options,
                'partners'      => $partners
            );
        }

        $data['logo'] = HTTP_CATALOG . 'image/' . $this->config->get('config_logo');

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $this->load->model('extension/module/event_manager/partner');

        $partners = $this->model_extension_module_event_manager_partner->getPartners();

        $data['partners'] = array();

        foreach ($partners as $partner) {

            $data['partners'][] = array(
                'partner_id'        => $partner['partner_id'],
                'name'              => $partner['name'],
                'recipe_value'      => $partner['recipe_value'],
                'recipe_type'       => $partner['recipe_type']
            );

        }

        $data['groups'] = array();

        $data['groups'][] = array(
            'text'  => $this->language->get('text_year'),
            'value' => 'year',
        );

        $data['groups'][] = array(
            'text'  => $this->language->get('text_month'),
            'value' => 'month',
        );

        $data['groups'][] = array(
            'text'  => $this->language->get('text_week'),
            'value' => 'week',
        );

        $data['groups'][] = array(
            'text'  => $this->language->get('text_day'),
            'value' => 'day',
        );

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_partner'])) {

            $url .= '&filter_partner=' . $this->request->get['filter_partner'];

        }

        if (isset($this->request->get['filter_report_type'])) {
            $url .= '&filter_report_type=' . $this->request->get['filter_report_type'];
        }

        if (isset($this->request->get['filter_company'])) {

            $url .= '&filter_company=' . urlencode($this->request->get['filter_company']);

        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('extension/module/event_manager/report', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_date_start'] = $filter_date_start;
        $data['filter_date_end'] = $filter_date_end;
        $data['filter_report_type'] = $filter_report_type;
        $data['filter_company'] = $filter_company;
        $data['filter_partner'] = $filter_partner;
        $data['filter_order_status_id'] = $filter_order_status_id;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager/report_list', $data));

    }

    public function getAllOrders() {

        $this->load->language('extension/module/event_manager/report');

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = date('Y-m-d');
        }

        if (isset($this->request->get['filter_report_type'])) {

            $filter_report_type = $this->request->get['filter_report_type'];

        } else {

            $filter_report_type = 'simple';

        }

        if (isset($this->request->get['filter_company'])) {

            $filter_company = $this->request->get['filter_company'];

        } else {

            $filter_company = '';

        }

        if (isset($this->request->get['filter_partner'])) {

            $filter_partner = $this->request->get['filter_partner'];

        } else {

            $filter_partner = 0;

        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $filter_order_status_id = $this->request->get['filter_order_status_id'];
        } else {
            $filter_order_status_id = 0;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->load->model('extension/report/sale');

        $data['orders'] = array();

        $filter_data = array(
            'filter_date_start'	     => $filter_date_start,
            'filter_date_end'	     => $filter_date_end,
            'filter_report_type'	 => $filter_report_type,
            'filter_company'	     => $filter_company,
            'filter_partner'	     => $filter_partner,
            'filter_order_status_id' => $filter_order_status_id
        );

        $this->load->model('extension/module/event_manager/report');

        $results = $this->model_extension_module_event_manager_report->getAllOrders($filter_data);

        $this->load->model('extension/module/event_manager/resend_document');

        foreach ($results as $result) {

            $options = $this->model_extension_module_event_manager_resend_document->getOrderProductOptions($result['order_id'], $result['order_product_id']);

            $partners = $this->model_extension_module_event_manager_report->getOrderProductPartner($result['product_id']);

            $data['orders'][] = array(
                'datetime'      => $result['date_added'],
                'name'          => $result['name'],
                'tax'           => $this->currency->format($result['tax'], $this->config->get('config_currency')),
                'price'         => $this->currency->format($result['price'], $this->config->get('config_currency')),
                'company'       => $result['company'],
                'options'       => $options,
                'partners'      => $partners
            );
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));

    }

}