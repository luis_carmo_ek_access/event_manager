<?php

class ControllerExtensionModuleEventManager extends Controller {

    private $error = array();

    const DEFAULT_MODULE_SETTINGS = [
        'name'      => 'Event Manager',
        'status'    => 1
    ];

    public function index() {

        if (isset($this->request->get['module_id'])) {

            $this->configure($this->request->get['module_id']);

        } else {

            $this->load->model('setting/module');
            $this->model_setting_module->addModule('event_manager', self::DEFAULT_MODULE_SETTINGS);
            $this->response->redirect($this->url->link('extension/module/event_manager', 'user_token=' . $this->session->data['user_token'] . 'module_id=' . $this->db->getLastId()));

        }

    }

    protected function configure($module_id) {

        $this->load->model('setting/module');
        $this->load->language('extension/module/event_manager');

        $this->document->setTitle($this->language->get('heading_title'));

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {

            $this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));

        }

        $data = array();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/event_manager', 'user_token=' . $this->session->data['user_token'], true)
        );

        $module_setting = $this->model_setting_module->getModule($module_id);

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } else {
            $data['status'] = $module_setting['status'];
        }

        $data['action']['cancel'] = $this->url->link('marketplace/extension', 'user_token='.$this->session->data['user_token'].'&type=module');
        $data['action']['save'] = "";

        $data['error'] = $this->error;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/event_manager', $data));

    }

    public function validate() {

        if (!$this->user->hasPermission('modify', 'extension/module/event_manager')) {

            $this->error['permission'] = true;
            return false;

        }

        if (!isset($this->request->post['status'])) {
            $this->error['status'] = true;
        }

        return empty($this->error);

    }

    public function install() {

        $this->load->model('setting/setting');
        $this->load->model('setting/module');
        $this->load->model('extension/module/event_manager');

        $this->model_setting_setting->editSetting('event_manager', ['module_event_manager_status'=>1]);

        $this->model_setting_module->addModule('event_manager', self::DEFAULT_MODULE_SETTINGS);

        $this->model_setting_event->addEvent('event_manager', 'admin/view/common/column_left/before', 'extension/module/event_manager/menus' );

        $this->model_extension_module_event_manager->install();

    }

    public function uninstall() {

        $this->load->model('setting/setting');
        $this->load->model('extension/module/event_manager');

        $this->model_setting_setting->deleteSetting('event_manager');

        $this->model_extension_module_event_manager->uninstall();

        $this->model_setting_event->deleteEventByCode('event_manager');

    }

    public function menus($eventRule, &$data) {

        $this->load->language('extension/module/event_manager');

        $event_manager = array();

        if ($this->user->hasPermission('access', 'extension/module/event_manager/partner')) {

            $event_manager[] = array(
                'name' => $this->language->get('heading_partner'),
                'href' => $this->url->link('extension/module/event_manager/partner', 'user_token=' . $this->session->data['user_token'], true),
                'children' => array()
            );

        }

        if ($this->user->hasPermission('access', 'extension/module/event_manager/resend_document')) {

            $event_manager[] = array(
                'name' => $this->language->get('heading_resend_document'),
                'href' => $this->url->link('extension/module/event_manager/resend_document', 'user_token=' . $this->session->data['user_token'], true),
                'children' => array()
            );

        }

        if ($this->user->hasPermission('access', 'extension/module/event_manager/report')) {

            $event_manager[] = array(
                'name' => $this->language->get('heading_report'),
                'href' => $this->url->link('extension/module/event_manager/report', 'user_token=' . $this->session->data['user_token'], true),
                'children' => array()
            );

        }

        if ($event_manager) {
            $data['menus'][] = array(
                'id'       => 'menu-event-manager',
                'icon'	   => 'fa fa-ticket',
                'name'	   => $this->language->get('heading_title'),
                'href'     => '',
                'children' => $event_manager
            );
        }

    }

    public function addOrderHistory($eventRule, &$data) {

        $this->log->write('SHOULD DO SOMETHING');

    }

}