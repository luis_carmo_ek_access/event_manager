<?php

// Text
$_['text_subject']                  = '%s - Order %s (Tickets)';
$_['text_welcome']                  = 'Welcome!';
$_['text_order_confirm']            = 'Confirmation of the acquisition of the following entries for the %s!';
$_['text_total']                    = 'Total: ';
$_['text_check_order']              = 'You can edit the information related on this order, until the start of the event, ';
$_['text_here']                     = 'here!';
$_['text_more_informations']        = 'For More Information:';
$_['text_voucher']                  = 'Your Voucher!';
$_['text_voucher_description']      = 'Lorem ipsum dolor sit amet, consectetuer adipsiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enum ad minim veniamm quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.';
$_['text_corporation']              = 'Corporation';