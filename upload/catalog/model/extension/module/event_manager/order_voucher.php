<?php

class ModelExtensionModuleEventManagerOrderVoucher extends Model {

    public function addVoucherToOrder($order_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_event_manager_voucher` WHERE order_id = '" . (int)$order_id . "'");

        $products = $this->db->query("
            SELECT
                *
            FROM
                " . DB_PREFIX . "order_product
            WHERE
                order_id = '" . (int)$order_id . "'
        ");

        $events_purchased = array();

        foreach ($products->rows as $product) {

            $enabled = $this->db->query("
                SELECT
                    *
                FROM
                    " . DB_PREFIX . "event_manager_product
                WHERE
                    product_id = '" . (int)$product['product_id'] . "'
            ");

            if ($enabled->num_rows > 0) {

                if ($enabled->row['status'] == true) {

                    for ($index = 0; $index < $product['quantity']; $index++) {

                        if (!in_array($enabled->row['event_name'], $events_purchased)) {

                            array_push($events_purchased, $enabled->row['event_name']);

                        }

                        $code = $order_id . $product['product_id'] . $index;

                        $code_length = strlen($code);

                        $missing_chars = $enabled->row['code_length'] - $code_length;

                        $missing_code = '';

                        for ($num=0; $num < $missing_chars; $num++) {

                            $missing_code .= rand(0,9);

                        }

                        $code .= $missing_code;

                        $this->db->query("
                            INSERT INTO 
                                " . DB_PREFIX . "order_event_manager_voucher
                            SET 
                                order_id = '" . $product['order_id'] . "', 
                                order_product_id = '" . $product['order_product_id'] . "', 
                                product_id = '" . $product['product_id'] . "', 
                                code = '" . $code . "', 
                                date_added = NOW()
                        ");

                    }

                }

            }

        }

        //////////////////////////////////////////

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($order_id);

        $language = new Language($order_info['language_code']);
        $language->load($order_info['language_code']);
        $language->load('extension/module/event_manager/send_document');

        $data['text_welcome']               = $language->get('text_welcome');
        $data['text_order_confirm']         = html_entity_decode(sprintf($language->get('text_order_confirm'), implode(" / ", $events_purchased)), ENT_QUOTES, 'UTF-8');
        $data['text_total']                 = $language->get('text_total');
        $data['text_check_order']           = $language->get('text_check_order');
        $data['text_here']                  = $language->get('text_here');
        $data['text_more_informations']     = $language->get('text_more_informations');
        $data['text_voucher']               = $language->get('text_voucher');
        $data['text_voucher_description']   = $language->get('text_voucher_description');
        $data['text_corporation']           = $language->get('text_corporation');

        $data['config_event_manager_email_header_image']        = HTTPS_SERVER . '/image/' . $this->config->get('config_event_manager_email_header_image');
        $data['config_event_manager_email_voucher_image']       = DIR_IMAGE . $this->config->get('config_event_manager_email_voucher_image');
        $data['config_event_manager_email_footer_website']      = $this->config->get('config_event_manager_email_footer_website');
        $data['config_event_manager_email_footer_email']        = $this->config->get('config_event_manager_email_footer_email');
        $data['config_event_manager_email_footer_telephone']    = $this->config->get('config_event_manager_email_footer_telephone');
        $data['config_event_manager_email_footer_background']   = $this->config->get('config_event_manager_email_footer_background');
        $data['config_event_manager_email_footer_text_color']   = $this->config->get('config_event_manager_email_footer_text_color');

        if ($order_info['customer_id']) {
            $data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_info['order_id'];
        } else {
            $data['link'] = '';
        }

        $order_products = $this->model_checkout_order->getOrderProducts($order_info['order_id']);

        // Attachments
        include_once('pdf.php');
        $attachments = array();

        // Products
        $data['products'] = array();

        foreach ($order_products as $order_product) {

            $option_data = array();

            $order_options = $this->model_checkout_order->getOrderOptions($order_info['order_id'], $order_product['order_product_id']);

            $order_attributes = $this->getProductAttributes($order_product['product_id']);

            foreach ($order_options as $order_option) {
                if ($order_option['type'] != 'file') {
                    $value = $order_option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($order_option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $order_option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            $data['products'][] = array(
                'name'     => $order_product['name'],
                'quantity' => $order_product['quantity'],
                'price'    => $this->currency->format($order_product['price'] + ($this->config->get('config_tax') ? $order_product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'option'   => $option_data,
                'total'    => $this->currency->format($order_product['total'] + ($this->config->get('config_tax') ? ($order_product['tax'] * $order_product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
            );

            $enabled = $this->db->query("
                SELECT
                    *
                FROM
                    " . DB_PREFIX . "event_manager_product
                WHERE
                    product_id = '" . (int)$product['product_id'] . "'
            ");

            if ($enabled->num_rows > 0) {

                if ($enabled->row['status'] == true) {

                    $code_query = $this->db->query("
                SELECT
                    code
                FROM
                    " . DB_PREFIX . "order_event_manager_voucher
                WHERE
                    order_id = '" . $order_info['order_id'] . "' 
                    AND
                    order_product_id = '" . $order_product['order_product_id'] . "'
                    AND
                    product_id = '" . $order_product['product_id'] . "'
            ");

                    $barcode = $code_query->row['code'];

                    $data['code'] = $barcode;
                    $data['product_attributes'] = $order_attributes;
                    $data['product_options'] = $order_options;
                    $data['corporation'] = $order_info['firstname'] . ' ' . $order_info['lastname'];

                    $code_info = $this->getProductCodeType($order_product['product_id']);

                    $code_type = $code_info['code_type'];

                    $event_manager_description = $this->getEventManagerDescription($order_product['product_id']);

                    $data['event_manager_description'] = $event_manager_description;

                    $file_name = md5(rand()) . '.pdf';

                    $pdf = new Pdf();

                    $options = $pdf->getOptions();

                    $options->set(array('isHtml5ParserEnabled' => true, 'isPhpEnabled' => true, 'isRemoteEnabled' => true));

                    $pdf->setOptions($options);

                    $contxt = stream_context_create([
                        'ssl' => [
                            'verify_peer' => FALSE,
                            'verify_peer_name' => FALSE,
                            'allow_self_signed' => TRUE
                        ]
                    ]);

                    $pdf->setHttpContext($contxt);

                    $template = $this->load->view('extension/module/event_manager/mail/document_template', $data);

                    $template = str_replace(HTTPS_SERVER . 'image/', DIR_IMAGE, $template);

                    include_once('barcode/src/BarcodeGenerator.php');
                    include_once('barcode/src/BarcodeGeneratorPNG.php');
                    include_once('barcode/src/BarcodeGeneratorSVG.php');
                    include_once('barcode/src/BarcodeGeneratorJPG.php');
                    include_once('barcode/src/BarcodeGeneratorHTML.php');

                    include_once('barcode/src/Exceptions/BarcodeException.php');
                    include_once('barcode/src/Exceptions/InvalidCharacterException.php');
                    include_once('barcode/src/Exceptions/InvalidCheckDigitException.php');
                    include_once('barcode/src/Exceptions/InvalidFormatException.php');
                    include_once('barcode/src/Exceptions/InvalidLengthException.php');
                    include_once('barcode/src/Exceptions/UnknownTypeException.php');

                    $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();

                    if ($code_info['status'] == 1) {

                        if ($code_type == 'text') {

                            $template = str_replace("(CODE)", '#' . $barcode, $template);

                        } elseif ($code_type == 'qrcode') {

                            $qr_name = md5(rand()) . '.png';

                            include_once('phpqrcode/qrlib.php');

                            if (!file_exists(DIR_IMAGE . $qr_name)) {
                                QRcode::png($barcode, DIR_IMAGE . $qr_name);
                            }

                            $template = str_replace("(CODE)", '<img src="' . HTTPS_SERVER . '/image/' . $qr_name . '">', $template);

                        } elseif ($code_type == 'TYPE_EAN_13') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_EAN_13)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_128') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_11') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_11)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_128_A') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128_A)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_128_B') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128_B)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_128_C') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128_C)) . '">', $template);

                        } elseif ($code_type == 'TYPE_EAN_2') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_EAN_2)) . '">', $template);

                        } elseif ($code_type == 'TYPE_EAN_5') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_EAN_5)) . '">', $template);

                        } elseif ($code_type == 'TYPE_EAN_8') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_EAN_8)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_39') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_39)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_39_CHECKSUM') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_39_CHECKSUM)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_39E') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_39E)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_39E_CHECKSUM') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_39E_CHECKSUM)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODE_93') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_93)) . '">', $template);

                        } elseif ($code_type == 'TYPE_STANDARD_2_5') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_STANDARD_2_5)) . '">', $template);

                        } elseif ($code_type == 'TYPE_STANDARD_2_5_CHECKSUM') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_STANDARD_2_5_CHECKSUM)) . '">', $template);

                        } elseif ($code_type == 'TYPE_INTERLEAVED_2_5') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_INTERLEAVED_2_5)) . '">', $template);

                        } elseif ($code_type == 'TYPE_INTERLEAVED_2_5_CHECKSUM') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_INTERLEAVED_2_5_CHECKSUM)) . '">', $template);

                        } elseif ($code_type == 'TYPE_UPC_A') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_UPC_A)) . '">', $template);

                        } elseif ($code_type == 'TYPE_UPC_E') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_UPC_E)) . '">', $template);

                        } elseif ($code_type == 'TYPE_MSI') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_MSI)) . '">', $template);

                        } elseif ($code_type == 'TYPE_MSI_CHECKSUM') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_MSI_CHECKSUM)) . '">', $template);

                        } elseif ($code_type == 'TYPE_POSTNET') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_POSTNET)) . '">', $template);

                        } elseif ($code_type == 'TYPE_PLANET') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_PLANET)) . '">', $template);

                        } elseif ($code_type == 'TYPE_RMS4CC') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_RMS4CC)) . '">', $template);

                        } elseif ($code_type == 'TYPE_KIX') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_KIX)) . '">', $template);

                        } elseif ($code_type == 'TYPE_IMB') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_IMB)) . '">', $template);

                        } elseif ($code_type == 'TYPE_CODABAR') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODABAR)) . '">', $template);

                        } elseif ($code_type == 'TYPE_PHARMA_CODE') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_PHARMA_CODE)) . '">', $template);

                        } elseif ($code_type == 'TYPE_PHARMA_CODE_TWO_TRACKS') {

                            $template = str_replace("(CODE)", '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode, $generator::TYPE_PHARMA_CODE_TWO_TRACKS)) . '">', $template);

                        } else {

                            $template = str_replace("(CODE)", '#' . $barcode, $template);

                        }

                    } else {

                        $template = str_replace("(CODE)", '#' . $barcode, $template);

                    }

                    $pdf->loadHtml($template, 'UTF-8');

                    $pdf->render();
                    $file = $pdf->output();
                    file_put_contents('catalog/model/extension/module/event_manager/pdf_files/' . $file_name, $file);

                    array_push($attachments, $file_name);

                }

            }

        }

        // Order Totals
        $data['totals'] = array();

        $order_totals = $this->model_checkout_order->getOrderTotals($order_info['order_id']);

        foreach ($order_totals as $order_total) {

            if ($order_total['code'] == 'total') {

                $data['totals'][] = array(
                    'title' => $order_total['title'],
                    'text' => $this->currency->format($order_total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

        }

        $this->load->model('setting/setting');

        $from = $this->model_setting_setting->getSettingValue('config_email', $order_info['store_id']);

        if (!$from) {
            $from = $this->config->get('config_email');
        }

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($order_info['email']);
        $mail->setFrom($from);
        $mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));

        foreach ($attachments as $attachment) {

            $mail->AddAttachment('catalog/model/extension/module/event_manager/pdf_files/' . $attachment);

        }

        $mail->setSubject(html_entity_decode(sprintf($language->get('text_subject'), $order_info['store_name'], $order_info['order_id']), ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($this->load->view('extension/module/event_manager/send_document', $data));
        $mail->send();

    }

    public function removeVoucherFromOrder($order_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_event_manager_voucher` WHERE order_id = '" . (int)$order_id . "'");

    }

    public function getProductAttributes($product_id) {

        $query = $this->db->query("
            SELECT
                attr_desc.name,
                prod_attr.text
            FROM
                " . DB_PREFIX . "product_attribute AS prod_attr
            LEFT JOIN
                " . DB_PREFIX . "attribute AS attr
                ON (
                    attr.attribute_id = prod_attr.attribute_id
                )
            LEFT JOIN
                " . DB_PREFIX . "attribute_description AS attr_desc
                ON (
                    attr_desc.attribute_id = attr.attribute_id
                )
            WHERE
                prod_attr.product_id = '" . (int)$product_id . "'
                AND
                attr_desc.language_id = '" . $this->config->get('config_language_id') . "'
                AND
                prod_attr.language_id = '" . $this->config->get('config_language_id') . "'
        ");

        return $query->rows;

    }

    public function getEventManagerDescription($product_id) {

        $query = $this->db->query("
                SELECT
                    product_id,
                    event_description
                FROM
                    " . DB_PREFIX . "event_manager_product
                WHERE
                    product_id = '" . (int)$product_id . "'
            ");

        return ($query->num_rows > 0) ? $query->row['event_description'] : '';

    }

    public function getProductCodeType($product_id) {

        $query = $this->db->query("
            SELECT
                code_type,
                status
            FROM
                " . DB_PREFIX . "event_manager_product
            WHERE
                product_id = '" . (int)$product_id . "'
        ");

        return ($query->num_rows > 0) ? $query->row : array();

    }

}